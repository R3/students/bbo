import os
def write_matlabfile(algorithm , cplex, loc='.', filename='bbo.m'):
    '''
        Write parameters according to given algorithm as parameter and write in to a matlab format file.

        USAGE : 
            write_matlabfile(1, cplex, path, filename)
        INPUTS :
            algorithm   :  The given algorithm to be selected when executing matlab code. 
                           In this case, it have to be one of the following : 
                           {'Automatic':0, 'Primal Simplex':1, 'Dual Simplex':2, 'Barrier':4, 'Network':3, 'Sifting':5, 'Concurrent':6}
            cplex       :  Object with all parameters depending on which algorithm has been selected ( see file bbo_declaration.py ).
            loc         :  Given path where file have to be stored. Default location is the current directory
            filename    :  Name of the matlab format file. Default filename is bbo.m
        OUTPUTS :
            None
        EXAMPLE :
            write_matlabfile(1, cplex)
            No given location and filename, so default will be used.
            write_matlabfile(1, cplex, "../project/", "project.m")
            Name of the file will be now project.m and stored in a folder called project.

    '''
    # parameter algo is a number which represents which algorithm should be executed, passed from bbo_optimisation.py
    # see bbo_optimisation.py
    f = open(os.path.join(loc, filename), 'w')
    algoCode = "param.lpmethod.Cur = " + str(algorithm) + ";\n" 
    # lpmethod.Cur parameter defined in MATLAB from CPLEX library decides which algorithm will be used
    # lpmethod.Cur = 1 : Primal Simplex
    f.write(algoCode)
    # write all parameters with param. structure to bbo.m 
    # parameters contain name and value
    for k in range(len(cplex.parameters.db)):
        f.write('param.' + cplex.parameters.db[k].name + ' = ' + str(cplex.parameters.db[k].get_default())  + ';\n')
    f.close()
def run(algorithm, cplex):
    '''
        Execute defined BBO optimization project (see blackbox.m) with cplex algorithm and parameters under OSX environment

        USAGE : 
            run(1, cplex)
        INPUTS :
            algorithm   :  The given algorithm to be selected when executing matlab code. 
                           In this case, it have to be one of the following : 
                           {'Automatic':0, 'Primal Simplex':1, 'Dual Simplex':2, 'Barrier':4, 'Network':3, 'Sifting':5, 'Concurrent':6}
            cplex       :  Object with all parameters depending on which algorithm has been selected ( see file bbo_declaration.py ).
        OUTPUTS
            None
        EXAMPLE :
            run(1, cplex)
            Execute blackbox problem with algorithm 1 which is Primal Simplex
    '''
    # preparation of file bbo.m and execute Blackbox.m file in matlab
    write_matlabfile(algorithm, cplex)
    # launch matlab (the blackbox script)
    os.system('/Applications/MATLAB_R2018b.app/bin/matlab < blackbox.m')


