## Black Box Optimization

## What is OPAL? OPAL---A Framework for Optimization of Algorithms

OPAL is a Python language based Framework allows to declare algorithm optimization which depend on parameters and affect the performance. Simple syntax permits optimization problem formulation and solution. 
### Requierements 
+ Python version 2.6 or 2.7 (not tested with 3.x)
+ [NOMAD](http://www.gerad.ca/NOMAD)

### Install
+ Unzip the package or clone the git repository
+ Go to the source directory
+ Run `python setup.py install`

### Testing 
To run the following test, [Numpy](http://www.numpy.org) is required. Install
it with `pip install numpy`.

Assuming your `PYTHONPATH` is set correctly, you should be able to do:

    cd examples/fd
    python fd_optimize.py
### References 
+ [Optimization of Algorithms with OPAL](http://dx.doi.org/10.1007/s12532-014-0067-x)
+ [Templating and Automatic Code Generation for Performance with Python](https://gerad.ca/en/papers/G-2011-30/view)
+ [Taking Advantage of Parallelism in Algorithmic Parameter Optimization](http://dx.doi.org/10.1007/s11590-011-0428-6)
+ [Algorithmic Parameter Optimization of the DFO Method with the OPAL Framework](http://dx.doi.org/10.1007/978-1-4419-6935-4_15)



## What is convert.py?
convert.py is a python source code which the input is a csv file containing specific parameters for the 5 IBM-CPLEX solver's algorithms. The output of convert.py contains parameter list converted from csv in py. format.

### Generation
+ read in csv_file and object definition as dictionary <br />
+ open writable python file 'bbo.declaration.py' to import Algorithm, Parameter, Measure <br />
+ write parameter keys associated with their values by concatenation into python file <br />
+ according numbers of algorithms, associate set of parameter list with strArray
+ save and close python file

###  Requirements 
+ Python version 2.6 or 2.7 (not tested with 3.x)
+ iPython version 2.6 or 2.7 

### References
+ [convert.py]
+ [bbo.declaration.py]



