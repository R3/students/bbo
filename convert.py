import csv # used for reading csv file with parameters
algorithms = ['Primal Simplex', 'Dual Simplex', 'Barrier', 'Network', 'Sifting', 'Concurrent'] # all algorithms supported by CPLEX library
nAlgorithms = len(algorithms) 
strArray = [''] * nAlgorithms # parameter rows from csv file will be put under string format and separated into strArray block according to specific algorithm

with open ('CPLEXParam_list.csv', mode='r') as csvfile: 
    CPLEXParam_list = csv.DictReader(csvfile, delimiter=',') # associate csvfile into dictionary
    f = open("bbo_declaration.py", "w") # create executable python file
    f.write("from opal.core.algorithm import Algorithm\n")
    f.write("from opal.core.parameter import Parameter\n")
    f.write("algorithms = {'Automatic': 0, 'Primal Simplex': 1, 'Dual Simplex': 2, 'Barrier': 4, 'Network' 3: , 'Sifting': 5, 'Concurrent': 6}\n")
    f.write("# Define new algorithms\n")
    f.write("CPLEX = Algorithm(name='CPLEX', description='Parameter Methods OPTimization')\n")
    f.write("# Register executable for CPELX.\n")
    f.write("CPLEX.set_executable_command('python bbo_run.py') #executable command to be run by OPAL\n")
    f.write("def declare(algorithm = algorithms['Automatic']):\n")
    f.write("# set all parameters according to the specific algorithms\n")
    f.write("\t# Add parameters\n")
    for row in CPLEXParam_list:
        value = row['\xef\xbb\xbfALGORITHM'] # save value for next correct key insertion
        del row['\xef\xbb\xbfALGORITHM'] # delete key : value cause key incorrect
        row['ALGORITHM'] = value # insert a new entry with correct key & associated value saved ealier
        if( not (row['C_param_name'] == '') and not (row['MATLAB_param_name'] == 'not available')): # don't take rows containing '' and not available
            for i in range(nAlgorithms):
                if row['ALGORITHM'] ==  algorithms[i] : # associate rows to the specific algorithm [i]
                    strArray[i] += "\t\tCPLEX.add_param(Parameter(name='" + row['MATLAB_param_name']+"', "+ "kind='" + row['kind '] +"'," + "bound=[" + row['lower bound']+','+ row['upper bound']+"]," + "default=" +row['default']+", " + "description='" + row['emphasizes precision ']+"'))"+"\n"
                        # rows with CPLEX.add_param structure followed by concatenating name, kind, bound, default and description 

    for i in range(nAlgorithms):
        if len(strArray[i]) > 0: # empty means that the i'th algorithm has no parameter in the csv file
            if i == 0:
                f.write("\tif ")
            else:
                f.write("\telif ")
            f.write("algorithm == algorithms['" + algorithms[i] + "']:\n")
        f.write(strArray[i])
        
    f.close() 
    
    
    