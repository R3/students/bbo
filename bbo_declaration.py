from opal.core.algorithm import Algorithm
from opal.core.parameter import Parameter
algorithms = {'Automatic': 0, 'Primal Simplex': 1, 'Dual Simplex': 2, 'Barrier': 4, 'Network' 3: , 'Sifting': 5, 'Concurrent': 6}
# Define new algorithms
CPLEX = Algorithm(name='CPLEX', description='Parameter Methods OPTimization')
# Register executable for CPELX.
CPLEX.set_executable_command('python bbo_run.py') #executable command to be run by OPAL
def declare(algorithm = algorithms['Automatic']):
# set all parameters according to the specific algorithms
	# Add parameters
	if algorithm == algorithms['Primal Simplex']:
		CPLEX.add_param(Parameter(name='advance', kind='integer',bound=[0,2],default=1, description='use advanced starting information'))
		CPLEX.add_param(Parameter(name='simplex.limits.lowerobj', kind='real',bound=[-1E+75,1E+75],default=-1E+75, description='lower limit than the value of the objective function'))
		CPLEX.add_param(Parameter(name='simplex.limits.upperobj', kind='real',bound=[-1E+75,1E+75],default=1E+75, description='upper limit than the value of the objective function'))
		CPLEX.add_param(Parameter(name='simplex.dgradient', kind='integer',bound=[0,5],default=0, description='type of pricing'))
		CPLEX.add_param(Parameter(name='simplex.pgradient', kind='integer',bound=[-1,4],default=0, description='primal simplex pricing '))
		CPLEX.add_param(Parameter(name='simplex.crash', kind='integer',bound=[-1,1],default=1, description='CPLEX orders variables relative to initial basis'))
		CPLEX.add_param(Parameter(name='simplex.tolerances.markowitz', kind='real',bound=[0.0001,0.99999],default=0.01, description='influences pivot selection'))
		CPLEX.add_param(Parameter(name='simplex.tolerance.optimality', kind='real',bound=[0.000000001,0.1],default=0.000001, description='reduced-cost tolerance for optimality'))
		CPLEX.add_param(Parameter(name='simplex.limits.perturbation', kind='real',bound=[0.00000001,0.1],default=0.000001, description='CPLEX perturbs upper and lower bounds '))
		CPLEX.add_param(Parameter(name='simplex.perturbation', kind='integer',bound=[0,1],default=0, description='decides wether to perturb problems'))
		CPLEX.add_param(Parameter(name='simplex.limits.perturbation', kind='integer',bound=[0,10],default=0, description='set number of degenrate iterations'))
		CPLEX.add_param(Parameter(name='feasopt.tolerance', kind='real',bound=[0.000000001,0.001],default=0.000001, description='controls amount of relaxation for routine CPXfeaspot'))
		CPLEX.add_param(Parameter(name='simplex.tolerances.feasibility', kind='real',bound=[0.000000001,0.000001],default=0.000001, description='specifies the feasibility tolerance'))
		CPLEX.add_param(Parameter(name='simplex.limits.iterations', kind='real',bound=[10,1E+75],default=9.22337E+18, description='set maximum number of simplex iterations'))
		CPLEX.add_param(Parameter(name='emphasis.memory', kind='integer',bound=[0,1],default=0, description='directs CPLEX should conserving memory'))
		CPLEX.add_param(Parameter(name='emphasis.numerical', kind='integer',bound=[0,1],default=0, description='emphasizes precision '))
		CPLEX.add_param(Parameter(name='simplex.pricing', kind='integer',bound=[0,10],default=0, description='set maximum number of variables of pricing candidates'))
		CPLEX.add_param(Parameter(name='sifting.algorithm', kind='integer',bound=[0,4],default=0, description='set algorithm for solving sifting subproblems'))
		CPLEX.add_param(Parameter(name='simplex.display', kind='integer',bound=[0,2],default=1, description='sets how often CPLEX reportsabout iterations'))
		CPLEX.add_param(Parameter(name='simplex.limits.singularity', kind='integer',bound=[0,10],default=10, description='restricts number of times CPLEX attempts'))
	elif algorithm == algorithms['Dual Simplex']:
		CPLEX.add_param(Parameter(name='advance', kind='integer',bound=[0,2],default=1, description='use advanced starting information'))
		CPLEX.add_param(Parameter(name='simplex.limits.lowerobj', kind='real',bound=[-1E+75,1E+75],default=-1E+75, description='lower limit than the value of the objective function'))
		CPLEX.add_param(Parameter(name='simplex.limits.upperobj', kind='real',bound=[-1E+75,1E+75],default=1E+75, description='upper limit than the value of the objective function'))
		CPLEX.add_param(Parameter(name='simplex.dgradient', kind='integer',bound=[0,5],default=0, description='type of pricing'))
		CPLEX.add_param(Parameter(name='simplex.pgradient', kind='integer',bound=[-1,4],default=0, description='primal simplex pricing '))
		CPLEX.add_param(Parameter(name='simplex.crash', kind='integer',bound=[-1,1],default=1, description='CPLEX orders variables relative to initial basis'))
		CPLEX.add_param(Parameter(name='simplex.tolerances.markowitz', kind='real',bound=[0.0001,0.99999],default=0.01, description='influences pivot selection'))
		CPLEX.add_param(Parameter(name='simplex.tolerance.optimality', kind='real',bound=[0.000000001,0.1],default=0.000001, description='reduced-cost tolerance for optimality'))
		CPLEX.add_param(Parameter(name='simplex.limits.perturbation', kind='real',bound=[0.00000001,0.1],default=0.000001, description='CPLEX perturbs upper and lower bounds '))
		CPLEX.add_param(Parameter(name='simplex.perturbation', kind='integer',bound=[0,1],default=0, description='decides wether to perturb problems'))
		CPLEX.add_param(Parameter(name='simplex.limits.perturbation', kind='integer',bound=[0,10],default=0, description='set number of degenrate iterations'))
		CPLEX.add_param(Parameter(name='feasopt.tolerance', kind='real',bound=[0.000000001,0.001],default=0.000001, description='controls amount of relaxation for routine CPXfeaspot'))
		CPLEX.add_param(Parameter(name='simplex.tolerances.feasibility', kind='real',bound=[0.000000001,0.000001],default=0.000001, description='specifies the feasibility tolerance'))
		CPLEX.add_param(Parameter(name='simplex.limits.iterations', kind='real',bound=[10,1E+75],default=9.22337E+18, description='set maximum number of simplex iterations'))
		CPLEX.add_param(Parameter(name='emphasis.memory', kind='integer',bound=[0,1],default=0, description='directs CPLEX should conserving memory'))
		CPLEX.add_param(Parameter(name='emphasis.numerical', kind='integer',bound=[0,1],default=0, description='emphasizes precision '))
		CPLEX.add_param(Parameter(name='simplex.pricing', kind='integer',bound=[0,10],default=0, description='set maximum number of variables of pricing candidates'))
		CPLEX.add_param(Parameter(name='sifting.algorithm', kind='integer',bound=[0,4],default=0, description='set algorithm for solving sifting subproblems'))
		CPLEX.add_param(Parameter(name='simplex.display', kind='integer',bound=[0,2],default=1, description='sets how often CPLEX reportsabout iterations'))
		CPLEX.add_param(Parameter(name='simplex.limits.singularity', kind='integer',bound=[0,10],default=10, description='restricts number of times CPLEX attempts'))
	elif algorithm == algorithms['Barrier']:
		CPLEX.add_param(Parameter(name='advance', kind='integer',bound=[0,2],default=1, description='use advanced starting information'))
		CPLEX.add_param(Parameter(name='barrier.algorithm', kind='integer',bound=[0,3],default=0, description='infeasibility-estimate start algorithm'))
		CPLEX.add_param(Parameter(name='barrier.startalg', kind='integer',bound=[1,4],default=1, description='sets algorithm used to compute the initial starting point'))
		CPLEX.add_param(Parameter(name='barrier.crossover', kind='integer',bound=[-1,2],default=1, description='decides which crossover is performed'))
		CPLEX.add_param(Parameter(name='sifting.algorithm', kind='integer',bound=[0,4],default=0, description='set algorithm used for solving sifting subproblems'))
		CPLEX.add_param(Parameter(name='barrier.ordering', kind='integer',bound=[0,3],default=0, description='set algorithm used to permute rows of the matrix'))
		CPLEX.add_param(Parameter(name='barrier.display', kind='integer',bound=[0,2],default=1, description='set levels of barrier progress information to be displayed'))
		CPLEX.add_param(Parameter(name='barrier.limits.growth', kind='real',bound=[1,1E+12],default=1E+12, description='to detected unbounded optimal faces'))
		CPLEX.add_param(Parameter(name='barrier.colnonzeros', kind='integer',bound=[0,10],default=0, description='recognition of dense columns'))
		CPLEX.add_param(Parameter(name='barrier.limits.iteration', kind='real',bound=[0,9.22E+36],default=9.22337E+18, description='sets number of barrier iterations before termination'))
		CPLEX.add_param(Parameter(name='barrier.limits.corrections', kind='integer',bound=[-1,10],default=-1, description='sets maximum number of centering corrections '))
		CPLEX.add_param(Parameter(name='barrier.limits.objrange', kind='real',bound=[0,1E+75],default=1E+20, description='sets maximum absolute value of the objective functioj'))
		CPLEX.add_param(Parameter(name='barrier.convergetol', kind='real',bound=[1E-12,1E+12],default=0.00000001, description='sets the tolerance on complementarity of convergence'))
		CPLEX.add_param(Parameter(name='barrier.qcpconvergetol', kind='real',bound=[1E-12,1E+12],default=0.0000001, description='sets the tolerance on complementarity of convergence in QCPs'))
		CPLEX.add_param(Parameter(name='emphasis.memory', kind='integer',bound=[0,1],default=0, description='directs CPLEX should conserving memory'))
		CPLEX.add_param(Parameter(name='emphasis.numerical', kind='integer',bound=[0,1],default=0, description='emphasizes precision '))
	elif algorithm == algorithms['Sifting']:
		CPLEX.add_param(Parameter(name='sifting.algorithm', kind='integer',bound=[0,4],default=0, description='set algorithm for solving sifting subproblems'))
		CPLEX.add_param(Parameter(name='sifting.display', kind='integer',bound=[0,2],default=1, description='sets amount of information to display'))
		CPLEX.add_param(Parameter(name='sifting.iterations', kind='real',bound=[10,1E+75],default=9.22337E+18, description='upper limit on sifting iterations'))
	elif algorithm == algorithms['Concurrent']:
		CPLEX.add_param(Parameter(name='advance', kind='integer',bound=[0,2],default=1, description='use advanced starting information'))
		CPLEX.add_param(Parameter(name='simplex.limits.lowerobj', kind='real',bound=[-1E+75,1E+75],default=-1E+75, description='lower limit than the value of the objective function'))
		CPLEX.add_param(Parameter(name='simplex.limits.upperobj', kind='real',bound=[-1E+75,1E+75],default=1E+75, description='upper limit than the value of the objective function'))
		CPLEX.add_param(Parameter(name='simplex.dgradient', kind='integer',bound=[0,5],default=0, description='type of pricing'))
		CPLEX.add_param(Parameter(name='simplex.pgradient', kind='integer',bound=[-1,4],default=0, description='primal simplex pricing '))
		CPLEX.add_param(Parameter(name='simplex.crash', kind='integer',bound=[-1,1],default=1, description='CPLEX orders variables relative to initial basis'))
		CPLEX.add_param(Parameter(name='simplex.tolerances.markowitz', kind='real',bound=[0.0001,0.99999],default=0.01, description='influences pivot selection'))
		CPLEX.add_param(Parameter(name='simplex.tolerance.optimality', kind='real',bound=[0.000000001,0.1],default=0.000001, description='reduced-cost tolerance for optimality'))
		CPLEX.add_param(Parameter(name='simplex.limits.perturbation', kind='real',bound=[0.00000001,0.1],default=0.000001, description='CPLEX perturbs upper and lower bounds '))
		CPLEX.add_param(Parameter(name='simplex.perturbation', kind='integer',bound=[0,1],default=0, description='decides wether to perturb problems'))
		CPLEX.add_param(Parameter(name='simplex.limits.perturbation', kind='integer',bound=[0,10],default=0, description='set number of degenrate iterations'))
		CPLEX.add_param(Parameter(name='feasopt.tolerance', kind='real',bound=[0.000000001,0.001],default=0.000001, description='controls amount of relaxation for routine CPXfeaspot'))
		CPLEX.add_param(Parameter(name='simplex.tolerances.feasibility', kind='real',bound=[0.000000001,0.000001],default=0.000001, description='specifies the feasibility tolerance'))
		CPLEX.add_param(Parameter(name='simplex.limits.iterations', kind='real',bound=[10,1E+75],default=9.22337E+18, description='set maximum number of simplex iterations'))
		CPLEX.add_param(Parameter(name='emphasis.memory', kind='integer',bound=[0,1],default=0, description='directs CPLEX should conserving memory'))
		CPLEX.add_param(Parameter(name='emphasis.numerical', kind='integer',bound=[0,1],default=0, description='emphasizes precision '))
		CPLEX.add_param(Parameter(name='simplex.pricing', kind='integer',bound=[0,10],default=0, description='set maximum number of variables of pricing candidates'))
		CPLEX.add_param(Parameter(name='sifting.algorithm', kind='integer',bound=[0,4],default=0, description='set algorithm for solving sifting subproblems'))
		CPLEX.add_param(Parameter(name='simplex.display', kind='integer',bound=[0,2],default=1, description='sets how often CPLEX reportsabout iterations'))
		CPLEX.add_param(Parameter(name='simplex.limits.singularity', kind='integer',bound=[0,10],default=10, description='restricts number of times CPLEX attempts'))
		CPLEX.add_param(Parameter(name='advance', kind='integer',bound=[0,2],default=1, description='use advanced starting information'))
		CPLEX.add_param(Parameter(name='barrier.algorithm', kind='integer',bound=[0,3],default=0, description='infeasibility-estimate start algorithm'))
		CPLEX.add_param(Parameter(name='barrier.startalg', kind='integer',bound=[1,4],default=1, description='sets algorithm used to compute the initial starting point'))
		CPLEX.add_param(Parameter(name='barrier.crossover', kind='integer',bound=[-1,2],default=1, description='decides which crossover is performed'))
		CPLEX.add_param(Parameter(name='sifting.algorithm', kind='integer',bound=[0,4],default=0, description='set algorithm used for solving sifting subproblems'))
		CPLEX.add_param(Parameter(name='barrier.ordering', kind='integer',bound=[0,3],default=0, description='set algorithm used to permute rows of the matrix'))
		CPLEX.add_param(Parameter(name='barrier.display', kind='integer',bound=[0,2],default=1, description='set levels of barrier progress information to be displayed'))
		CPLEX.add_param(Parameter(name='barrier.limits.growth', kind='real',bound=[1,1E+12],default=1E+12, description='to detected unbounded optimal faces'))
		CPLEX.add_param(Parameter(name='barrier.colnonzeros', kind='integer',bound=[0,10],default=0, description='recognition of dense columns'))
		CPLEX.add_param(Parameter(name='barrier.limits.iteration', kind='real',bound=[0,9.22E+36],default=9.22337E+18, description='sets number of barrier iterations before termination'))
		CPLEX.add_param(Parameter(name='barrier.limits.corrections', kind='integer',bound=[-1,10],default=-1, description='sets maximum number of centering corrections '))
		CPLEX.add_param(Parameter(name='barrier.limits.objrange', kind='real',bound=[0,1E+75],default=1E+20, description='sets maximum absolute value of the objective functioj'))
		CPLEX.add_param(Parameter(name='barrier.convergetol', kind='real',bound=[1E-12,1E+12],default=0.00000001, description='sets the tolerance on complementarity of convergence'))
		CPLEX.add_param(Parameter(name='barrier.qcpconvergetol', kind='real',bound=[1E-12,1E+12],default=0.0000001, description='sets the tolerance on complementarity of convergence in QCPs'))
		CPLEX.add_param(Parameter(name='emphasis.memory', kind='integer',bound=[0,1],default=0, description='directs CPLEX should conserving memory'))
		CPLEX.add_param(Parameter(name='emphasis.numerical', kind='integer',bound=[0,1],default=0, description='emphasizes precision '))
