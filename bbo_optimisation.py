from bbo_declaration import algorithms
from bbo_declaration import CPLEX
from bbo_declaration import declare
from bbo_run import run
# Selected algorithms (not all algorithms declared in CPLEX see write_matlabfile function in bbo_run) to be run with the specific model and FBA in Blackbox.m
algoToBeRun = ['Primal Simplex', 'Dual Simplex', 'Barrier','Sifting', 'Concurrent']

for algoName in algoToBeRun:
    algoValue = algorithms[algoName] # convert algorithm name into it's correspondant integer value, it's a dictionary defined in bbo_declaration
    declare(algoValue)
    run(algoValue, CPLEX)

# display execution time of each algorithm applied
# file time.txt will be created from Blackbox.m
file = open("time.txt", "r")
for line in file :
     if(line != '\n') and (line != ''):
        print(line[:-1])

