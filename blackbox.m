initCobraToolbox;  
% Model setup
% default solver glpk
global CBTDIR
modelFileName = '/Users/anna/cobratoolbox/Recon3D_301/Recon3DModel_301.mat'; % specific location for BBO optimisation project under OSX environment
model = readCbModel(modelFileName); 
FBAaerobic = optimizeCbModel(model, 'max') 
changeCobraSolver('ibm_cplex','all'); % change to ibm_cplex solver

run('bbo.m') % run the FBA with model and algorithm and parameters from bbo file.
tic; % start chronometer
solution = solveCobraLP(model, param) % solve the CobraLP with model and param as inputs
timeinterval = toc; # stop chronometer, timeinterval is the time used by the solver

fileID = fopen('time.txt', 'a') % create readable and writable document to store the time execution
content = fscanf(fileID, "%c") % read content of the file 
fprintf(fileID, content) % rewrite all content readed into the same file
fprintf(fileID,strcat('\n',solution.algorithm, " : ", num2str(solution.time))); % append the run time obtained under string form
fclose(fileID)

































